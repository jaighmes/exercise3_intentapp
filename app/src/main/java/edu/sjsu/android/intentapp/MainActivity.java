package edu.sjsu.android.intentapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button:
                String myUriString = "http://www.amazon.com";
                Intent myActivity = new Intent(Intent.ACTION_VIEW, Uri.parse(myUriString));
                Intent chooser = Intent.createChooser(myActivity, "Open with...");
                try {
                    startActivity(chooser);
                } catch (ActivityNotFoundException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.button2:
                String myPhoneNumberUri = "tel:+194912344444";
                Intent myActivity2 = new Intent(Intent.ACTION_DIAL, Uri.parse(myPhoneNumberUri));
                try {
                    startActivity(myActivity2);
                } catch (ActivityNotFoundException e) {
                    e.printStackTrace();
                }
                break;
            default:
                break;
        }
    }
}